package eu.slachta.zmd;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.event.IVideoPictureEvent;
import com.xuggle.xuggler.Global;

public class MediaScreenCapture {
	/*
	 * The gap in seconds between frames
	 */
	public double SECONDS_BETWEEN_FRAMES = 10;
	/*
	 * Calculated gap computed from SECONDS_BETWEEN_FRAMES
	 */
	public long MICRO_SECONDS_BETWEEN_FRAMES = (long) (Global.DEFAULT_PTS_PER_SECOND * SECONDS_BETWEEN_FRAMES);
	/*
	 * Input media stream or a media file
	 */
	private String inputFilename;
	/*
	 * outputdir path
	 */
	private File path;
	/*
	 * verbosity enables messages from events
	 */
	private boolean verbose;
	/*
	 * The video stream index, used to ensure we display frames from one and
	 * only one video stream from the media container.
	 */
	private static int mVideoStreamIndex = -1;
	/*
	 * Time of last frame write
	 */
	private static long mLastPtsWrite = Global.NO_PTS;

	/*
	 * List of comparable images
	 */

	private File originalImg;
	private File pathForMarked;
	private String hostname;
	private String username;
	private String password;
	private String workingDir;

	/**
	 * Creates an object of a Client class. It creates snapshots of input media
	 * file or a stream into a set of snapshots into specific folder specified
	 * by outputFilePrefix.
	 * 
	 * @param inputFileName
	 *            Input file or a stream (avi, mpg, rtsp://)
	 * @param outputFilePrefix
	 *            Output prefix specified by a path and a file prefix
	 *            (D:\path\example)
	 */

	public MediaScreenCapture(String inputFileName, File path, int ScreenGap,
			boolean verbose, File pathForMarked, String hostname,
			String username, String password, String workingDir) {

		this.workingDir = workingDir;
		this.hostname = hostname;
		this.username = username;
		this.password = password;
		this.inputFilename = inputFileName;
		this.pathForMarked = pathForMarked;
		this.verbose = verbose;
		this.path = path;
		this.SECONDS_BETWEEN_FRAMES = ScreenGap;

		if(path.isDirectory()){			
			IMediaReader mediaReader = ToolFactory.makeReader(this.inputFilename);

			mediaReader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
			mediaReader.addListener(new ImageSnapListener());
			try {
				while (mediaReader.readPacket() == null)
					;
			} catch (Exception e) {
				System.out.println("Xuggler library crash, stacktrace:");
				e.printStackTrace();
			}
		}
	}

	private class ImageSnapListener extends MediaListenerAdapter {
		public void onVideoPicture(IVideoPictureEvent event) {
			if (event.getStreamIndex() != mVideoStreamIndex) {
				if (mVideoStreamIndex == -1)
					mVideoStreamIndex = event.getStreamIndex();
				else
					return;
			}

			if (mLastPtsWrite == Global.NO_PTS) {
				mLastPtsWrite = event.getTimeStamp()
						- MICRO_SECONDS_BETWEEN_FRAMES;
			}

			if (event.getTimeStamp() - mLastPtsWrite >= MICRO_SECONDS_BETWEEN_FRAMES) {

				String outputFilename = dumpImageToFile(event.getImage());
				// indicate file written
				double seconds = ((double) event.getTimeStamp())
						/ Global.DEFAULT_PTS_PER_SECOND;

				if (verbose) {
					System.out.printf(
							"at elapsed time of %6.3f seconds wrote: %s\n",
							seconds, outputFilename);
				}

				// update last write time
				mLastPtsWrite += MICRO_SECONDS_BETWEEN_FRAMES;
			}

		}

		private String dumpImageToFile(BufferedImage image) {
			try {
				long prom = System.currentTimeMillis();

				String outputFilename = path.getCanonicalPath()
						+ System.getProperty("file.separator") + prom + ".jpg";

				File outputfile = new File(outputFilename);
				ImageIO.write(image, "jpg", outputfile);

				if (originalImg == null) {
					originalImg = outputfile;
				} else {
					if (!compareTwoImages(originalImg, outputfile)) {
						System.out.println("images are not the same");
						String outputMarkedFilePath = pathForMarked.getAbsolutePath()
								+ System.getProperty("file.separator")
								+ prom
								+ ".jpg";

						File outputfileMarked = new File(outputMarkedFilePath);
						ImageIO.write(image, "jpg", outputfileMarked);

						Thread ftp = new Thread(new FTPSender(outputfileMarked,hostname, username, password, workingDir,verbose));
						ftp.setDaemon(true); // necekat na konec vlakna a pokracovat dale..
						ftp.start();
						
						System.out.println("deleted output file");
						outputfile.delete();
					} else {
						System.out.println("images are the same");
						outputfile.delete();
					}
				}

				return outputFilename;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

	}

	/**
	 * Returns true, if those images are the same. Returns false, if they are
	 * different.
	 * 
	 * @param firstImage
	 * @param secondImage
	 * @return
	 */

	private boolean compareTwoImages(File firstImage, File secondImage) {
		ImageCompare ic = new ImageCompare(firstImage.getAbsolutePath(), secondImage.getAbsolutePath());

		ic.setParameters(8, 6, 5, 10);
		if (this.verbose) ic.setDebugMode(2);
		
		ic.compare();

		if (this.verbose) System.out.println("Match: " + ic.match());
		
		if (ic.match()) {
			return true;
		} else {
			return false;
		}
	}

}
