package eu.slachta.zmd;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.util.Arrays;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Main {
	static File path;
	static int screenGap = 5;
	static boolean verbose = true;
	private static String ftppassword;
	private static String ftphostname;
	private static String ftpusername;
	private static String ftpworkingdir;
	private static GridBagLayout gbLayout;
	private static GridBagConstraints gbConst;
	private static JLabel labelStream;
	private static JTextField fieldStream;
	private static JLabel labelGap;
	private static JTextField fieldGap;
	private static JTextField fieldSnapDir;
	private static JLabel labelFTPusername;
	private static JTextField fieldFTPusername;
	private static JLabel labelFTPpassword;
	private static JPasswordField fieldFTPpassword;
	private static JLabel labelFTPaddr;
	private static JTextField fieldFTPaddr;
	private static JLabel labelFTPdir;
	private static JTextField fieldFTPdir;
	private static String media;
	private static int gap;
	private static JLabel labelVerbose;
	private static JCheckBox checkVerbose;

	public static void main(String[] args) {
		try {
			  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException ex) {
				  // TODO
			} catch (IllegalAccessException ex) {
				  // TODO
			} catch (InstantiationException ex) {
				  // TODO
			} catch (UnsupportedLookAndFeelException ex) {
				  // TODO
			}

        gbLayout = new GridBagLayout();
        gbConst = new GridBagConstraints();
        gbConst.fill = GridBagConstraints.BOTH;

        gbConst.gridx = 0;
        gbConst.gridy = 0;
        gbConst.fill = GridBagConstraints.BOTH;
        gbConst.weightx = 1.0;
        gbConst.weighty = 1.0;
		
		JPanel panel = new JPanel();
		panel.setLayout(gbLayout);
		
        labelStream = new JLabel("RTSP stream: ");
        gbConst.gridwidth = 1;
        gbConst.insets.left = 0;
        gbConst.insets.right = 0;
        gbConst.insets.top = 5;
        gbConst.gridx = 0;
        gbConst.gridy = 0;
        panel.add(labelStream, gbConst);

        fieldStream = new JTextField("rtsp://streaming1.osu.edu/media2/ufsap/ufsap.mov");
        gbConst.gridx = 1;
        gbConst.gridy = 0;
        panel.add(fieldStream, gbConst);
        
        labelGap = new JLabel("Time gap between frames [s]: ");
        gbConst.gridx = 0;
        gbConst.gridy = 1;
        panel.add(labelGap, gbConst);

        fieldGap = new JTextField("10");
        gbConst.gridx = 1;
        gbConst.gridy = 1;
        panel.add(fieldGap, gbConst);
        
        labelGap = new JLabel("Directory for snapshots: ");
        gbConst.gridx = 0;
        gbConst.gridy = 2;
        panel.add(labelGap, gbConst);

        String snap = System.getProperty( "user.home" )+File.separator+"snapshots";
        fieldSnapDir = new JTextField(snap);
        gbConst.gridx = 1;
        gbConst.gridy = 2;
        panel.add(fieldSnapDir, gbConst);
        
        labelVerbose = new JLabel("Verbose: ");
        gbConst.gridx = 0;
        gbConst.gridy = 3;
        panel.add(labelVerbose, gbConst);

        checkVerbose = new JCheckBox();
        gbConst.gridx = 1;
        gbConst.gridy = 3;
        panel.add(checkVerbose, gbConst);
        
        labelFTPaddr = new JLabel("FTP hostname: ");
        gbConst.gridx = 0;
        gbConst.gridy = 4;
        panel.add(labelFTPaddr, gbConst);

        fieldFTPaddr = new JTextField("ftp.myhosting.cz");
        gbConst.gridx = 1;
        gbConst.gridy = 4;
        panel.add(fieldFTPaddr, gbConst);

        labelFTPusername = new JLabel("FTP username: ");
        gbConst.gridx = 0;
        gbConst.gridy = 5;
        panel.add(labelFTPusername, gbConst);

        fieldFTPusername = new JTextField("username");
        gbConst.gridx = 1;
        gbConst.gridy = 5;
        panel.add(fieldFTPusername, gbConst);
        
        labelFTPpassword = new JLabel("FTP password: ");
        gbConst.gridx = 0;
        gbConst.gridy = 6;
        panel.add(labelFTPpassword, gbConst);

        fieldFTPpassword = new JPasswordField("");
        gbConst.gridx = 1;
        gbConst.gridy = 6;
        panel.add(fieldFTPpassword, gbConst);
        
        labelFTPdir = new JLabel("FTP destination directory: ");
        gbConst.gridx = 0;
        gbConst.gridy = 7;
        panel.add(labelFTPdir, gbConst);

        fieldFTPdir = new JTextField("/my/path");
        gbConst.gridx = 1;
        gbConst.gridy = 7;
        panel.add(fieldFTPdir, gbConst);
        
		int result = JOptionPane.showConfirmDialog(null, panel, "ZMD APP", JOptionPane.OK_CANCEL_OPTION);
		
		if (result != JOptionPane.OK_OPTION) {
			JOptionPane.showMessageDialog(panel, "Quitting!", "Quitting!", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		
		verbose = checkVerbose.isSelected();
		
		if(verbose) System.out.println("params:");
		
		ftppassword = new String(fieldFTPpassword.getPassword());
		if(verbose) System.out.println("FTP password: "+ftppassword);
		ftphostname = fieldFTPaddr.getText();
		if(verbose) System.out.println("FTP hostname: "+ftphostname);
		ftpusername = fieldFTPusername.getText();
		if(verbose) System.out.println("FTP username: "+ftpusername);
		ftpworkingdir = fieldFTPdir.getText();
		if(verbose) System.out.println("FTP dir: "+ftpworkingdir);
		path = new File(fieldSnapDir.getText());
		if(verbose) System.out.println("snapshot dir: "+path.toString());
		media = fieldStream.getText();
		if(verbose) System.out.println("media stream: "+media);
		gap = Integer.parseInt(fieldGap.getText());
		if(verbose) System.out.println("snapshot gap: "+gap);
		if(verbose) System.out.println("ukecanej: "+((verbose)?"ano":"ne"));
		if(verbose) System.out.println("---------------------------");
		
		@SuppressWarnings("unused")
		ZmdCamMonitor a = new ZmdCamMonitor(
					media, 
					path,
					gap, 
					verbose, 
					ftphostname,
					ftpusername, 
					ftppassword, 
					ftpworkingdir);
	}
}
