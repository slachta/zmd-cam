package eu.slachta.zmd;

import java.io.File;

public class ZmdCamMonitor {
	/*
	 * input video stream/file
	 */
	private String input;
	/*
	 * General path for snapshot directory
	 */
	private File path;
	/*
	 * screenGap as an interval between two snapshots
	 */
	private int screenGap;
	/*
	 * verbosity for app
	 */
	private boolean verbose = false;

	/*
	 * Paths
	 */
	private File pathForMarkedFiles;
	private File pathForAllSnapshots;

	/*
	 * Directory names
	 */
	private String NAME_FOLDER_FOR_MARKED_CHANGE = "marked";
	private String NAME_FOLDER_FOR_ALL_SNAPSHOTS = "all";

	public ZmdCamMonitor(String inputStream, File path, int screenGap, boolean verbose,String ftphostname, String ftpusername, String ftppassword, String workingdir) {
		init(path, screenGap, inputStream, verbose);

		System.out.println("Output for monitor MediaScreenCapture:");
		@SuppressWarnings("unused")
		MediaScreenCapture a = new MediaScreenCapture(this.input, this.pathForAllSnapshots, this.screenGap, this.verbose, this.pathForMarkedFiles,ftphostname, ftpusername, ftppassword, workingdir);
	}

	private void init(File path, int screenGap, String inputStream, boolean verbose) {
		this.input = inputStream;
		this.screenGap = screenGap;
		this.verbose = verbose;
		
		System.out.println("Path check (ok if output is empty):");
		if (path.isDirectory()) {
			// ok, entered path is a directory
			this.path = path;
		} else if (path.isFile()){
			// Entered path is a file, get parent to get parent dir
			this.path = new File(path.getParent());
		} else {
			this.path = new File(path.getAbsolutePath());
			if(this.verbose) System.out.println("Missing path for all data, creating one - "+this.path);
			File dir = new File(this.path.getAbsolutePath());
			dir.mkdir();
		}

		// creates a folder for images that are marked as those with changes
		this.pathForMarkedFiles = new File(this.path.getAbsolutePath() + System.getProperty("file.separator")+ NAME_FOLDER_FOR_MARKED_CHANGE);

		// creates a dir for marked images if not exists
		if (!(pathForMarkedFiles.exists() && pathForMarkedFiles.isDirectory())) {
			if(this.verbose) System.out.println("Missing path for marked snapshots, creating one - "+this.pathForMarkedFiles.toString());
			File dir = new File(pathForMarkedFiles.getAbsolutePath());
			dir.mkdir();
		}

		this.pathForAllSnapshots = new File(this.path.getAbsolutePath()
				+ System.getProperty("file.separator")
				+ NAME_FOLDER_FOR_ALL_SNAPSHOTS);

		// creates a dir for marked images if not exists
		if (!(this.pathForAllSnapshots.exists() && this.pathForAllSnapshots.isDirectory())) {
			if(this.verbose) System.out.println("Missing path for all snapshots, creating one - "+this.pathForAllSnapshots.toString());
			File dir = new File(this.pathForAllSnapshots.getAbsolutePath());
			dir.mkdir();
		}
		System.out.println("---------------------------");
	}

}
