package eu.slachta.zmd;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

public class FTPSender implements Runnable{
	private String workingDirectory;
	private int ftpStatus;
	private File f;
	private String hostname;
	private String username;
	private String password;
	private boolean verbose;

	public FTPSender(File f, String hostname, String username, String password, String workingDirectory, boolean verbose) {
		this.workingDirectory = workingDirectory;
		this.f = f;
		this.hostname = hostname;
		this.username = username;
		this.password = password;
		this.verbose = verbose;
	}
	
	public void run(){
		String filename = f.getName();
		Long timestamp = Long.parseLong(f.getName().substring(0,f.getName().lastIndexOf(".")));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp);

		String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		String month = Integer.toString(cal.get(Calendar.MONTH)+1); // mesice jsou od nuly
		String year = Integer.toString(cal.get(Calendar.YEAR));
		FTPClient ftp = null;

		InputStream in = null;
		try {
			ftp = new FTPClient();
			ftp.setDefaultPort(21);
			ftp.enterLocalPassiveMode();
			ftp.connect(hostname);
			ftp.login(username, password);
			
			ftpStatus = ftp.getReplyCode();

			if (FTPReply.isPositiveCompletion(ftpStatus)) {
				in = new FileInputStream(f);

				if (!(this.workingDirectory.charAt(this.workingDirectory
						.lastIndexOf("/")) == this.workingDirectory.length())) {
					this.workingDirectory += "/";
				}
						
				if (verbose) System.out.println("FTP: cwd to "+this.workingDirectory);
				ftp.cwd(this.workingDirectory);
				
				if (verbose) System.out.println("FTP: make dir and cwd to "+this.workingDirectory + year);
				ftp.makeDirectory(year);
				ftp.cwd(this.workingDirectory + year);
				
				if (verbose) System.out.println("FTP: make dir and cwd to "+this.workingDirectory + year+"/"+month);
				ftp.makeDirectory(month);
				ftp.cwd(this.workingDirectory + year + "/" + month);
				
				if (verbose) System.out.println("FTP: make dir and cwd to "+this.workingDirectory + year+"/"+month+"/"+day);
				ftp.makeDirectory(day);
				ftp.cwd(this.workingDirectory + year + "/" + month + "/" + day);

				if (verbose) System.out.println("FTP: filename to store - "+filename);
				if (verbose) System.out.println("FTP: file to store - "+f.getAbsolutePath());
				ftp.storeFile(filename, in);
				
				if (verbose) System.out.println("FTP: sent, logging out");
				
				ftp.logout();
				if (verbose) System.out.println("FTP: logged out, disconnecting");
				ftp.disconnect();
				if (verbose) System.out.println("FTP: disconnected");
			} else {
				System.out.println("Possible authentication error. Please follow this FTP status code: "+ this.ftpStatus);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
